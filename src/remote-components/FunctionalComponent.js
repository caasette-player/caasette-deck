"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var React = window.React;
var Link = window.ReactRouterDOM.Link;
var Router = window.ReactRouterDOM.BrowserRouter;
var Route = window.ReactRouterDOM.Route;

// Adding comment

exports.default = function () {
  return React.createElement(
    "div",
    null,
    React.createElement(
      "div",
      null,
      "This is a functional component"
    )
  );
};