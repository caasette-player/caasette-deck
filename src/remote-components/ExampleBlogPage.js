(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('react')) :
  typeof define === 'function' && define.amd ? define(['exports', 'react'], factory) :
  (factory((global['@caasette'] = global['@caasette'] || {}, global['@caasette'].ExampleBlogPage = {}),global.React));
}(this, (function (exports,React) { 'use strict';

  var React__default = 'default' in React ? React['default'] : React;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    if (superClass) _setPrototypeOf(subClass, superClass);
  }

  function _getPrototypeOf(o) {
    _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf(o);
  }

  function _setPrototypeOf(o, p) {
    _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };

    return _setPrototypeOf(o, p);
  }

  function _assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return self;
  }

  function _possibleConstructorReturn(self, call) {
    if (call && (typeof call === "object" || typeof call === "function")) {
      return call;
    }

    return _assertThisInitialized(self);
  }

  var questions = [{
    title: "I dunno anymore",
    value: 'docNotLoading'
  }, {
    title: 'The text is too small',
    value: 'textTooSmall'
  }, {
    title: 'My information is wrong',
    value: 'informationWrong'
  }, {
    title: "I can't find where to sign",
    value: 'findWhereToSign'
  }, {
    title: 'Other',
    value: 'other'
  }];

  var EsignFeedbackForm =
  /*#__PURE__*/
  function (_Component) {
    _inherits(EsignFeedbackForm, _Component);

    function EsignFeedbackForm(props) {
      var _this;

      _classCallCheck(this, EsignFeedbackForm);

      _this = _possibleConstructorReturn(this, _getPrototypeOf(EsignFeedbackForm).call(this, props));
      var initialResponses = {};
      questions.map(function (question) {
        initialResponses[question.value] = false;
      });
      _this.state = {
        responses: initialResponses,
        feedbackText: ''
      };
      return _this;
    }

    _createClass(EsignFeedbackForm, [{
      key: "handleCheckboxChange",
      value: function handleCheckboxChange(value) {
        this.setState(function (prevState) {
          prevState.responses[value] = !prevState.responses[value];
          return prevState;
        });
      }
    }, {
      key: "handleTextAreaChange",
      value: function handleTextAreaChange(event) {
        this.setState({
          feedbackText: event.target.value
        });
      }
    }, {
      key: "submit",
      value: function submit() {
        alert("Feedback Text: ".concat(this.state.feedbackText, " Selected Responses: ").concat(this.selectedResponses));
      }
    }, {
      key: "cancel",
      value: function cancel() {
        this.setState({
          feedbackText: ''
        });
        this.setState(function (prevState) {
          questions.map(function (question) {
            prevState.responses[question.value] = false;
          });
        });
      }
    }, {
      key: "render",
      value: function render() {
        var _this2 = this;

        var feedbackText = this.state.feedbackText;
        return React__default.createElement("div", null, React__default.createElement("div", null, questions.map(function (question) {
          return React__default.createElement("div", {
            key: "".concat(question.value, "_").concat(question.title)
          }, React__default.createElement("label", {
            htmlFor: "answer",
            onClick: function onClick() {
              return _this2.handleCheckboxChange(question.value);
            }
          }, question.title, React__default.createElement("input", {
            type: "checkbox",
            checked: _this2.state.responses[question.value],
            onChange: function onChange() {
              return _this2.handleCheckboxChange(question.value);
            },
            defaultValue: question.value,
            name: "answer"
          })));
        })), this.isOtherSelected ? React__default.createElement("textarea", {
          placeholder: "Type your feedback here...",
          value: feedbackText,
          onChange: function onChange(event) {
            return _this2.handleTextAreaChange(event);
          },
          name: "feedbackTextName",
          "data-test-id": "textarea"
        }) : null, React__default.createElement("div", null, React__default.createElement("button", {
          onClick: function onClick() {
            return _this2.cancel();
          }
        }, "Cancel"), React__default.createElement("button", {
          onClick: function onClick() {
            return _this2.submit();
          }
        }, "Submit")));
      }
    }, {
      key: "selectedResponses",
      get: function get() {
        var _this3 = this;

        return questions.map(function (question) {
          if (_this3.state.responses[question.value]) {
            return question.value;
          }
        }).filter(function (value) {
          return !!value;
        });
      }
    }, {
      key: "isOtherSelected",
      get: function get() {
        return this.state.responses.other === true;
      }
    }]);

    return EsignFeedbackForm;
  }(React.Component); // EsignFeedbackForm.propTypes = {

  exports.EsignFeedbackForm = EsignFeedbackForm;
  exports.default = EsignFeedbackForm;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRXhhbXBsZUJsb2dQYWdlLmpzIiwic291cmNlcyI6WyIuLi9wYWNrYWdlcy9ibG9nLXBhZ2Uvc3JjL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qIGVzbGludC1kaXNhYmxlIGFycmF5LWNhbGxiYWNrLXJldHVybiAqL1xuLyogZXNsaW50LWRpc2FibGUgY29uc2lzdGVudC1yZXR1cm4gKi9cbi8qIGVzbGludC1kaXNhYmxlIG5vLWFsZXJ0ICovXG4vKiBlc2xpbnQtZGlzYWJsZSBuby1wYXJhbS1yZWFzc2lnbiAqL1xuLyogZXNsaW50LWRpc2FibGUgcmVhY3QvYnV0dG9uLWhhcy10eXBlICovXG4vKiBlc2xpbnQtZGlzYWJsZSByZWFjdC9kZXN0cnVjdHVyaW5nLWFzc2lnbm1lbnQgKi9cbi8qIGVzbGludC1kaXNhYmxlIHJlYWN0L25vLXVudXNlZC1wcm9wLXR5cGVzICovXG4vLyBpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnXG5cbi8vIGNvbnN0IGJ1dHRvbkNsaWNrID0gKCkgPT4gYWxlcnQoJ1lvdSBjbGlja2VkIHRoZSBidXR0b24nKVxuXG4vLyBleHBvcnQgZGVmYXVsdCAoKSA9PiAoXG4vLyAgIDxidXR0b24gb25DbGljaz17KCkgPT4gYnV0dG9uQ2xpY2soKX0+XG4vLyAgICAgQnV0dG9uXG4vLyAgIDwvYnV0dG9uPlxuLy8gKVxuXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnXG4vLyBpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnXG5cbmNvbnN0IHF1ZXN0aW9ucyA9IFtcbiAge1xuICAgIHRpdGxlOiBcIkkgZHVubm8gYW55bW9yZVwiLFxuICAgIHZhbHVlOiAnZG9jTm90TG9hZGluZycsXG4gIH0sXG4gIHtcbiAgICB0aXRsZTogJ1RoZSB0ZXh0IGlzIHRvbyBzbWFsbCcsXG4gICAgdmFsdWU6ICd0ZXh0VG9vU21hbGwnLFxuICB9LFxuICB7XG4gICAgdGl0bGU6ICdNeSBpbmZvcm1hdGlvbiBpcyB3cm9uZycsXG4gICAgdmFsdWU6ICdpbmZvcm1hdGlvbldyb25nJyxcbiAgfSxcbiAge1xuICAgIHRpdGxlOiBcIkkgY2FuJ3QgZmluZCB3aGVyZSB0byBzaWduXCIsXG4gICAgdmFsdWU6ICdmaW5kV2hlcmVUb1NpZ24nLFxuICB9LFxuICB7XG4gICAgdGl0bGU6ICdPdGhlcicsXG4gICAgdmFsdWU6ICdvdGhlcicsXG4gIH0sXG5dXG5cbmNsYXNzIEVzaWduRmVlZGJhY2tGb3JtIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcylcbiAgICBjb25zdCBpbml0aWFsUmVzcG9uc2VzID0ge31cbiAgICBxdWVzdGlvbnMubWFwKChxdWVzdGlvbikgPT4geyBpbml0aWFsUmVzcG9uc2VzW3F1ZXN0aW9uLnZhbHVlXSA9IGZhbHNlIH0pXG5cbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgcmVzcG9uc2VzOiBpbml0aWFsUmVzcG9uc2VzLFxuICAgICAgZmVlZGJhY2tUZXh0OiAnJyxcbiAgICB9XG4gIH1cblxuICBnZXQgc2VsZWN0ZWRSZXNwb25zZXMoKSB7XG4gICAgcmV0dXJuIHF1ZXN0aW9ucy5tYXAoKHF1ZXN0aW9uKSA9PiB7XG4gICAgICBpZiAodGhpcy5zdGF0ZS5yZXNwb25zZXNbcXVlc3Rpb24udmFsdWVdKSB7XG4gICAgICAgIHJldHVybiBxdWVzdGlvbi52YWx1ZVxuICAgICAgfVxuICAgIH0pLmZpbHRlcigodmFsdWUpID0+ICEhdmFsdWUpXG4gIH1cblxuICBnZXQgaXNPdGhlclNlbGVjdGVkKCkge1xuICAgIHJldHVybiB0aGlzLnN0YXRlLnJlc3BvbnNlcy5vdGhlciA9PT0gdHJ1ZVxuICB9XG5cbiAgaGFuZGxlQ2hlY2tib3hDaGFuZ2UodmFsdWUpIHtcbiAgICB0aGlzLnNldFN0YXRlKChwcmV2U3RhdGUpID0+IHtcbiAgICAgIHByZXZTdGF0ZS5yZXNwb25zZXNbdmFsdWVdID0gIXByZXZTdGF0ZS5yZXNwb25zZXNbdmFsdWVdXG4gICAgICByZXR1cm4gcHJldlN0YXRlXG4gICAgfSlcbiAgfVxuXG4gIGhhbmRsZVRleHRBcmVhQ2hhbmdlKGV2ZW50KSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGZlZWRiYWNrVGV4dDogZXZlbnQudGFyZ2V0LnZhbHVlIH0pXG4gIH1cblxuICBzdWJtaXQoKSB7XG4gICAgYWxlcnQoYEZlZWRiYWNrIFRleHQ6ICR7dGhpcy5zdGF0ZS5mZWVkYmFja1RleHR9IFNlbGVjdGVkIFJlc3BvbnNlczogJHt0aGlzLnNlbGVjdGVkUmVzcG9uc2VzfWApXG4gIH1cblxuICBjYW5jZWwoKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGZlZWRiYWNrVGV4dDogJycgfSlcbiAgICB0aGlzLnNldFN0YXRlKChwcmV2U3RhdGUpID0+IHtcbiAgICAgIHF1ZXN0aW9ucy5tYXAoKHF1ZXN0aW9uKSA9PiB7XG4gICAgICAgIHByZXZTdGF0ZS5yZXNwb25zZXNbcXVlc3Rpb24udmFsdWVdID0gZmFsc2VcbiAgICAgIH0pXG4gICAgfSlcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGZlZWRiYWNrVGV4dCB9ID0gdGhpcy5zdGF0ZVxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2PlxuICAgICAgICA8ZGl2PlxuICAgICAgICAgIHtxdWVzdGlvbnMubWFwKChxdWVzdGlvbikgPT4gKFxuICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICBrZXk9e2Ake3F1ZXN0aW9uLnZhbHVlfV8ke3F1ZXN0aW9uLnRpdGxlfWB9XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxsYWJlbFxuICAgICAgICAgICAgICAgIGh0bWxGb3I9XCJhbnN3ZXJcIlxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHRoaXMuaGFuZGxlQ2hlY2tib3hDaGFuZ2UocXVlc3Rpb24udmFsdWUpfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAge3F1ZXN0aW9uLnRpdGxlfVxuICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgdHlwZT1cImNoZWNrYm94XCJcbiAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e3RoaXMuc3RhdGUucmVzcG9uc2VzW3F1ZXN0aW9uLnZhbHVlXX1cbiAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoKSA9PiB0aGlzLmhhbmRsZUNoZWNrYm94Q2hhbmdlKHF1ZXN0aW9uLnZhbHVlKX1cbiAgICAgICAgICAgICAgICAgIGRlZmF1bHRWYWx1ZT17cXVlc3Rpb24udmFsdWV9XG4gICAgICAgICAgICAgICAgICBuYW1lPVwiYW5zd2VyXCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L2xhYmVsPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgKSl9XG4gICAgICAgIDwvZGl2PlxuICAgICAgICB7dGhpcy5pc090aGVyU2VsZWN0ZWRcbiAgICAgICAgICA/IChcbiAgICAgICAgICAgIDx0ZXh0YXJlYVxuICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIlR5cGUgeW91ciBmZWVkYmFjayBoZXJlLi4uXCJcbiAgICAgICAgICAgICAgdmFsdWU9e2ZlZWRiYWNrVGV4dH1cbiAgICAgICAgICAgICAgb25DaGFuZ2U9eyhldmVudCkgPT4gdGhpcy5oYW5kbGVUZXh0QXJlYUNoYW5nZShldmVudCl9XG4gICAgICAgICAgICAgIG5hbWU9XCJmZWVkYmFja1RleHROYW1lXCJcbiAgICAgICAgICAgICAgZGF0YS10ZXN0LWlkPVwidGV4dGFyZWFcIlxuICAgICAgICAgICAgLz4pIDogbnVsbH1cbiAgICAgICAgPGRpdj5cbiAgICAgICAgICA8YnV0dG9uIG9uQ2xpY2s9eygpID0+IHRoaXMuY2FuY2VsKCl9PlxuICAgICAgICAgICAgQ2FuY2VsXG4gICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgPGJ1dHRvbiBvbkNsaWNrPXsoKSA9PiB0aGlzLnN1Ym1pdCgpfT5cbiAgICAgICAgICAgIFN1Ym1pdFxuICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIClcbiAgfVxufVxuXG4vLyBFc2lnbkZlZWRiYWNrRm9ybS5wcm9wVHlwZXMgPSB7XG4vLyAgIG9uU3VibWl0OiBQcm9wVHlwZXMuZnVuYyxcbi8vICAgb25DYW5jZWxDbGljazogUHJvcFR5cGVzLmZ1bmMsXG4vLyAgIHBvbGljeU51bWJlcjogUHJvcFR5cGVzLnN0cmluZyxcbi8vICAgcGhvbmVOdW1iZXI6IFByb3BUeXBlcy5zdHJpbmcsXG4vLyB9XG5cbi8vIEVzaWduRmVlZGJhY2tGb3JtLmRlZmF1bHRQcm9wcyA9IHtcbi8vICAgcGhvbmVOdW1iZXI6ICcxLTg0NC0yMDYtMDU4NycsXG4vLyAgIG9uU3VibWl0OiAoKSA9PiB7IH0sXG4vLyAgIG9uQ2FuY2VsQ2xpY2s6ICgpID0+IHsgfSxcbi8vICAgcG9saWN5TnVtYmVyOiAnJyxcbi8vIH1cblxuZXhwb3J0IHsgRXNpZ25GZWVkYmFja0Zvcm0gfVxuZXhwb3J0IGRlZmF1bHQgRXNpZ25GZWVkYmFja0Zvcm1cbiJdLCJuYW1lcyI6WyJxdWVzdGlvbnMiLCJ0aXRsZSIsInZhbHVlIiwiRXNpZ25GZWVkYmFja0Zvcm0iLCJwcm9wcyIsImluaXRpYWxSZXNwb25zZXMiLCJtYXAiLCJxdWVzdGlvbiIsInN0YXRlIiwicmVzcG9uc2VzIiwiZmVlZGJhY2tUZXh0Iiwic2V0U3RhdGUiLCJwcmV2U3RhdGUiLCJldmVudCIsInRhcmdldCIsImFsZXJ0Iiwic2VsZWN0ZWRSZXNwb25zZXMiLCJSZWFjdCIsImhhbmRsZUNoZWNrYm94Q2hhbmdlIiwiaXNPdGhlclNlbGVjdGVkIiwiaGFuZGxlVGV4dEFyZWFDaGFuZ2UiLCJjYW5jZWwiLCJzdWJtaXQiLCJmaWx0ZXIiLCJvdGhlciIsIkNvbXBvbmVudCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7RUFvQkEsSUFBTUEsU0FBUyxHQUFHLENBQ2hCO0VBQ0VDLEVBQUFBLEtBQUssRUFBRSxpQkFEVDtFQUVFQyxFQUFBQSxLQUFLLEVBQUU7RUFGVCxDQURnQixFQUtoQjtFQUNFRCxFQUFBQSxLQUFLLEVBQUUsdUJBRFQ7RUFFRUMsRUFBQUEsS0FBSyxFQUFFO0VBRlQsQ0FMZ0IsRUFTaEI7RUFDRUQsRUFBQUEsS0FBSyxFQUFFLHlCQURUO0VBRUVDLEVBQUFBLEtBQUssRUFBRTtFQUZULENBVGdCLEVBYWhCO0VBQ0VELEVBQUFBLEtBQUssRUFBRSw0QkFEVDtFQUVFQyxFQUFBQSxLQUFLLEVBQUU7RUFGVCxDQWJnQixFQWlCaEI7RUFDRUQsRUFBQUEsS0FBSyxFQUFFLE9BRFQ7RUFFRUMsRUFBQUEsS0FBSyxFQUFFO0VBRlQsQ0FqQmdCLENBQWxCOztNQXVCTUM7Ozs7O0VBQ0osNkJBQVlDLEtBQVosRUFBbUI7RUFBQTs7RUFBQTs7RUFDakIsMkZBQU1BLEtBQU47RUFDQSxRQUFNQyxnQkFBZ0IsR0FBRyxFQUF6QjtFQUNBTCxJQUFBQSxTQUFTLENBQUNNLEdBQVYsQ0FBYyxVQUFDQyxRQUFELEVBQWM7RUFBRUYsTUFBQUEsZ0JBQWdCLENBQUNFLFFBQVEsQ0FBQ0wsS0FBVixDQUFoQixHQUFtQyxLQUFuQztFQUEwQyxLQUF4RTtFQUVBLFVBQUtNLEtBQUwsR0FBYTtFQUNYQyxNQUFBQSxTQUFTLEVBQUVKLGdCQURBO0VBRVhLLE1BQUFBLFlBQVksRUFBRTtFQUZILEtBQWI7RUFMaUI7RUFTbEI7Ozs7MkNBY29CUixPQUFPO0VBQzFCLFdBQUtTLFFBQUwsQ0FBYyxVQUFDQyxTQUFELEVBQWU7RUFDM0JBLFFBQUFBLFNBQVMsQ0FBQ0gsU0FBVixDQUFvQlAsS0FBcEIsSUFBNkIsQ0FBQ1UsU0FBUyxDQUFDSCxTQUFWLENBQW9CUCxLQUFwQixDQUE5QjtFQUNBLGVBQU9VLFNBQVA7RUFDRCxPQUhEO0VBSUQ7OzsyQ0FFb0JDLE9BQU87RUFDMUIsV0FBS0YsUUFBTCxDQUFjO0VBQUVELFFBQUFBLFlBQVksRUFBRUcsS0FBSyxDQUFDQyxNQUFOLENBQWFaO0VBQTdCLE9BQWQ7RUFDRDs7OytCQUVRO0VBQ1BhLE1BQUFBLEtBQUssMEJBQW1CLEtBQUtQLEtBQUwsQ0FBV0UsWUFBOUIsa0NBQWtFLEtBQUtNLGlCQUF2RSxFQUFMO0VBQ0Q7OzsrQkFFUTtFQUNQLFdBQUtMLFFBQUwsQ0FBYztFQUFFRCxRQUFBQSxZQUFZLEVBQUU7RUFBaEIsT0FBZDtFQUNBLFdBQUtDLFFBQUwsQ0FBYyxVQUFDQyxTQUFELEVBQWU7RUFDM0JaLFFBQUFBLFNBQVMsQ0FBQ00sR0FBVixDQUFjLFVBQUNDLFFBQUQsRUFBYztFQUMxQkssVUFBQUEsU0FBUyxDQUFDSCxTQUFWLENBQW9CRixRQUFRLENBQUNMLEtBQTdCLElBQXNDLEtBQXRDO0VBQ0QsU0FGRDtFQUdELE9BSkQ7RUFLRDs7OytCQUVRO0VBQUE7O0VBQUEsVUFDQ1EsWUFERCxHQUNrQixLQUFLRixLQUR2QixDQUNDRSxZQUREO0VBRVAsYUFDRU8sMENBQ0VBLDBDQUNHakIsU0FBUyxDQUFDTSxHQUFWLENBQWMsVUFBQ0MsUUFBRDtFQUFBLGVBQ2JVO0VBQ0UsVUFBQSxHQUFHLFlBQUtWLFFBQVEsQ0FBQ0wsS0FBZCxjQUF1QkssUUFBUSxDQUFDTixLQUFoQztFQURMLFdBR0VnQjtFQUNFLFVBQUEsT0FBTyxFQUFDLFFBRFY7RUFFRSxVQUFBLE9BQU8sRUFBRTtFQUFBLG1CQUFNLE1BQUksQ0FBQ0Msb0JBQUwsQ0FBMEJYLFFBQVEsQ0FBQ0wsS0FBbkMsQ0FBTjtFQUFBO0VBRlgsV0FJR0ssUUFBUSxDQUFDTixLQUpaLEVBS0VnQjtFQUNFLFVBQUEsSUFBSSxFQUFDLFVBRFA7RUFFRSxVQUFBLE9BQU8sRUFBRSxNQUFJLENBQUNULEtBQUwsQ0FBV0MsU0FBWCxDQUFxQkYsUUFBUSxDQUFDTCxLQUE5QixDQUZYO0VBR0UsVUFBQSxRQUFRLEVBQUU7RUFBQSxtQkFBTSxNQUFJLENBQUNnQixvQkFBTCxDQUEwQlgsUUFBUSxDQUFDTCxLQUFuQyxDQUFOO0VBQUEsV0FIWjtFQUlFLFVBQUEsWUFBWSxFQUFFSyxRQUFRLENBQUNMLEtBSnpCO0VBS0UsVUFBQSxJQUFJLEVBQUM7RUFMUCxVQUxGLENBSEYsQ0FEYTtFQUFBLE9BQWQsQ0FESCxDQURGLEVBc0JHLEtBQUtpQixlQUFMLEdBRUdGO0VBQ0UsUUFBQSxXQUFXLEVBQUMsNEJBRGQ7RUFFRSxRQUFBLEtBQUssRUFBRVAsWUFGVDtFQUdFLFFBQUEsUUFBUSxFQUFFLGtCQUFDRyxLQUFEO0VBQUEsaUJBQVcsTUFBSSxDQUFDTyxvQkFBTCxDQUEwQlAsS0FBMUIsQ0FBWDtFQUFBLFNBSFo7RUFJRSxRQUFBLElBQUksRUFBQyxrQkFKUDtFQUtFLHdCQUFhO0VBTGYsUUFGSCxHQVFTLElBOUJaLEVBK0JFSSwwQ0FDRUE7RUFBUSxRQUFBLE9BQU8sRUFBRTtFQUFBLGlCQUFNLE1BQUksQ0FBQ0ksTUFBTCxFQUFOO0VBQUE7RUFBakIsa0JBREYsRUFJRUo7RUFBUSxRQUFBLE9BQU8sRUFBRTtFQUFBLGlCQUFNLE1BQUksQ0FBQ0ssTUFBTCxFQUFOO0VBQUE7RUFBakIsa0JBSkYsQ0EvQkYsQ0FERjtFQTBDRDs7OzBCQWhGdUI7RUFBQTs7RUFDdEIsYUFBT3RCLFNBQVMsQ0FBQ00sR0FBVixDQUFjLFVBQUNDLFFBQUQsRUFBYztFQUNqQyxZQUFJLE1BQUksQ0FBQ0MsS0FBTCxDQUFXQyxTQUFYLENBQXFCRixRQUFRLENBQUNMLEtBQTlCLENBQUosRUFBMEM7RUFDeEMsaUJBQU9LLFFBQVEsQ0FBQ0wsS0FBaEI7RUFDRDtFQUNGLE9BSk0sRUFJSnFCLE1BSkksQ0FJRyxVQUFDckIsS0FBRDtFQUFBLGVBQVcsQ0FBQyxDQUFDQSxLQUFiO0VBQUEsT0FKSCxDQUFQO0VBS0Q7OzswQkFFcUI7RUFDcEIsYUFBTyxLQUFLTSxLQUFMLENBQVdDLFNBQVgsQ0FBcUJlLEtBQXJCLEtBQStCLElBQXRDO0VBQ0Q7Ozs7SUF0QjZCQzs7Ozs7Ozs7Ozs7OzsifQ==
