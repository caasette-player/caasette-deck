require('babel-polyfill')

import React from 'react';
import Caasette from '@caasette-player/loadable'

import 'core-js/fn/object/assign'

// I wonder if this actually works. Will re-consider later.
// The problem here is that I can't delete it from the page but that isn't a big deal no?
export const removeCaasettes = () => {
  delete window['@caasette']
}

export const removeCaasette = (caasette) => {
  delete window['@caasette'][caasette]
}

class App extends React.Component {
  constructor(props) {
    super(props)
    this.Route = window.ReactRouterDOM.Route
    this.Router = window.ReactRouterDOM.BrowserRouter
  }

  render = () => (
    <div>
      <header>
        <img src="https://i.imgur.com/JP8jl2l.png" alt="Yeoman Generator" />
        <button type="button" onClick={removeCaasettes}>Reomve Caasettes from memory (not the page)</button>
      </header>
      <div className="column" style={{'backgroundColor':'#aaa'}}>
        <h2>Try out a simple Button</h2>
        <div><Caasette caasette="@caasette/BlogPost/Simple" /></div>
      </div>
      <div className="column" style={{'backgroundColor':'#bbb'}}>
        <h2>A remote form component</h2>
        <div><Caasette caasette="@caasette/ExampleBlogPage" /></div>
      </div>
      <div className="column" style={{'backgroundColor':'#ccc'}}>
        <h2>Some coolio text</h2>
        <div><Caasette caasette="@caasette/ReactRectanglePopupMenu/Simple" /></div>
      </div>
      <footer>
        <p><code>caasette-player</code></p>
        <p>Contact information: <a href="#">someone@example.com</a>.</p>
      </footer>
    </div>
  )
}

window.onload = () => {
  window.ReactDOM.render(<App />, document.getElementById('app'))
}

// import waitForReact from './caasette-loadable/WaitForReact'
// Render the main component into the dom once React is available.
// waitForReact.then(() => {
//   window.ReactDOM.render(<App />, document.getElementById('app'))
// })
