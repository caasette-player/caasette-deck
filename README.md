# Loads & plays Caasettes

> A simple `caasette-player` implimentation.

![](https://images.unsplash.com/photo-1524779709304-40b5a3560c60?ixlib=rb-0.3.5&s=efd7dc5fbc44b44d0f338619dfc4b22b&auto=format&fit=crop&w=1869&q=80)


## Usage
Locally
```js
yarn install
yarn start
```

In your record-store as a "Master Container"
```js
// hurr durr
```

Modifying it's template to do that good good
```js
// hurr durr
```

---
Planned features roadmap:
* https://bitbucket.org/snippets/caasette-player/nejyMo
